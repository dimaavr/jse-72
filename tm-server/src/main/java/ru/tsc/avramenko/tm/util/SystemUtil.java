package ru.tsc.avramenko.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface SystemUtil {

    static long getPID() {
        @Nullable
        final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            }
            catch (@NotNull Exception e) {
                return 0;
            }
        }
        return 0;
    }

    @NotNull
    static String getCurrentDateTime() {
        @NotNull final String pattern = "dd.mm.yyyy HH:MM:SS";
        @NotNull final SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat(pattern);
        @NotNull final String date = simpleDateFormat.format(new Date());
        return date;
    }

}