package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.marker.SoapCategory;

import java.util.List;

public class ProjectEndpointTest {

    @NotNull
    private final static SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final static SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final static ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final static ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Nullable
    private static SessionDTO session;

    @Nullable
    private ProjectDTO project;

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.openSession("User", "User");
        projectEndpoint.clearProject(session);
    }

    @Before
    public void before() {
        projectEndpoint.createProject(session, "Project1", "ProjectDesc1");
        this.project = projectEndpoint.findProjectByName(session, "Project1");
    }

    @After
    public void after() {
        projectEndpoint.clearProject(session);
    }

    @AfterClass
    public static void afterClass() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void findProjectAll() {
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectAll(session);
        Assert.assertNotNull(projects);
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectById() {
        @NotNull final ProjectDTO projectNew = projectEndpoint.finishProjectById(session, project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.COMPLETED, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectByName() {
        @NotNull final ProjectDTO projectNew = projectEndpoint.finishProjectByName(session, project.getName());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.COMPLETED, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectByIndex() {
        @NotNull final ProjectDTO project = projectEndpoint.finishProjectByIndex(session, 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusById() {
        projectEndpoint.setProjectStatusById(session, project.getId(), Status.NOT_STARTED);
        @NotNull final ProjectDTO projectNew = projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.NOT_STARTED, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByName() {
        projectEndpoint.setProjectStatusByName(session, project.getName(), Status.NOT_STARTED);
        @NotNull final ProjectDTO projectNew = projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.NOT_STARTED, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByIndex() {
        projectEndpoint.setProjectStatusByIndex(session, 0, Status.NOT_STARTED);
        @NotNull final ProjectDTO projectNew = projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectById() {
        @NotNull final ProjectDTO projectNew = projectEndpoint.startProjectById(session, project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.IN_PROGRESS, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectByName() {
        @NotNull final ProjectDTO projectNew = projectEndpoint.startProjectByName(session, project.getName());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(Status.IN_PROGRESS, projectNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectByIndex() {
        @NotNull final ProjectDTO project = projectEndpoint.startProjectByIndex(session, 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateProjectById() {
        @NotNull final ProjectDTO projectNew = projectEndpoint.updateProjectById(session, project.getId(), "NewProjectName", "NewProjectDesc");
        Assert.assertNotNull(projectNew);
        Assert.assertEquals("NewProjectName", projectNew.getName());
        Assert.assertEquals("NewProjectDesc", projectNew.getDescription());
    }

}