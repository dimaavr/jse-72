package ru.tsc.avramenko.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.listener.AbstractListener;
import ru.tsc.avramenko.tm.event.ConsoleEvent;

@Component
public class ExitListener extends AbstractListener {

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    @EventListener(condition = "@exitListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.exit(0);
    }

}