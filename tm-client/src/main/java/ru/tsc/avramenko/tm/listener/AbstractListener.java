package ru.tsc.avramenko.tm.listener;

import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.endpoint.Role;
import ru.tsc.avramenko.tm.event.ConsoleEvent;

public abstract class AbstractListener {

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void handler(@NotNull final ConsoleEvent event);

    public Role[] roles() {
        return null;
    }


    @Override
    public String toString() {
        String result = "";
        String name = name();
        String arg = arg();
        String description = description();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "]";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}