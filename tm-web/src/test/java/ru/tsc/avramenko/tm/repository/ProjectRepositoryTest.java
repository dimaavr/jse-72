package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.configuration.DataBaseConfiguration;
import ru.tsc.avramenko.tm.marker.WebCategory;
import ru.tsc.avramenko.tm.model.Project;

import java.util.Collection;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class ProjectRepositoryTest {

    @NotNull
    private static final String USER_ID = "123-456-789";

    @Autowired
    private ProjectRepository projectRepository;

    @Before
    public void before() {
        projectRepository.removeAllByCurrentUserId(USER_ID);
    }

    @Test
    @Category(WebCategory.class)
    public void findAllByCurrentUserId() {
        @NotNull final Project project = new Project("project");
        project.setUserId(USER_ID);
        projectRepository.save(project);
        @Nullable final Collection<Project> projectNew = projectRepository.findAllByCurrentUserId(USER_ID);
        Assert.assertEquals(1, projectNew.size());
    }

    @Test
    @Category(WebCategory.class)
    public void findByIdAndUserId() {
        @NotNull final Project project = new Project("project");
        project.setUserId(USER_ID);
        projectRepository.save(project);
        @Nullable final Project projectNew = projectRepository.findByIdAndUserId(project.getId(), USER_ID);
        Assert.assertNotNull(projectNew);
    }

    @Test
    @Category(WebCategory.class)
    public void removeByIdAndUserId() {
        @NotNull final Project project = new Project("project");
        project.setUserId(USER_ID);
        projectRepository.save(project);
        projectRepository.removeByIdAndUserId(project.getId(), USER_ID);
        Assert.assertEquals(0, projectRepository.findAllByCurrentUserId(USER_ID).size());
    }

    @Test
    @Category(WebCategory.class)
    public void removeAllByCurrentUserId() {
        @NotNull final Project project = new Project("project");
        @NotNull final Project project2 = new Project("project2");
        project.setUserId(USER_ID);
        project2.setUserId(USER_ID);
        projectRepository.save(project);
        projectRepository.save(project2);
        final Integer size = projectRepository.findAllByCurrentUserId(USER_ID).size();
        projectRepository.removeAllByCurrentUserId(USER_ID);
        final Integer size1 = projectRepository.findAllByCurrentUserId(USER_ID).size();
        Assert.assertNotEquals(size, size1);
    }

}
