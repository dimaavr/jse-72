package ru.tsc.avramenko.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public enum RoleType {

    ADMIN("Administrator"),
    USER("User");

    @NotNull
    private final String displayName;

}