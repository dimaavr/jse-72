package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.avramenko.tm.model.Project;

import java.util.Collection;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    @Query("SELECT e FROM Project e WHERE e.userId = :userId")
    Collection<Project> findAllByCurrentUserId(
            @Param("userId") final String userId
    );

    @Query("SELECT e FROM Project e WHERE e.id = :id and e.userId = :userId")
    Project findByIdAndUserId(
            @Param("id") final String id,
            @Param("userId") final String userId
    );

    @Modifying
    @Query("DELETE FROM Project e WHERE e.id = :id and e.userId = :userId")
    void removeByIdAndUserId(
            @Param("id") @NotNull String id,
            @Param("userId") @NotNull String userId
    );

    @Modifying
    @Query("DELETE FROM Project e WHERE e.userId = :userId")
    void removeAllByCurrentUserId(
            @Param("userId") @NotNull String userId
    );

}