package ru.tsc.avramenko.tm.exception.system;

import ru.tsc.avramenko.tm.exception.AbstractException;

public class ProcessException extends AbstractException {

    public ProcessException() {
        super("Process error! Try again.");
    }

}