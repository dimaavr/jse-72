package ru.tsc.avramenko.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "result")
@XmlAccessorType(XmlAccessType.FIELD)
public class Result {

    @Nullable
    public String messageResult = null;

    @NotNull
    private Boolean success = true;

    public Result(@NotNull final Boolean success) {
        this.success = success;
    }

    public Result(@NotNull final User user) {
        this.messageResult = user.getLogin();
    }

    public Result(@NotNull final Exception e) {
        success = false;
        messageResult = e.getMessage();
    }

}