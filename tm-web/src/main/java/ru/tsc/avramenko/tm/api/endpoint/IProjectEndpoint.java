package ru.tsc.avramenko.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @WebMethod
    @GetMapping("/find/{id}")
    Project find(@PathVariable("id") final String id);

    @WebMethod
    @GetMapping("/findAll")
    Collection<Project> findAll();

    @WebMethod
    @PostMapping("/create")
    Project create(@RequestBody final Project project);

    @WebMethod
    @PostMapping("/createAll")
    List<Project> createAll(@RequestBody final List<Project> projects);

    @WebMethod
    @PostMapping("/save")
    Project save(@RequestBody final Project project);

    @WebMethod
    @PostMapping("/saveAll")
    List<Project> saveAll(@RequestBody final List<Project> projects);

    @WebMethod
    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") final String id);

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll();

}