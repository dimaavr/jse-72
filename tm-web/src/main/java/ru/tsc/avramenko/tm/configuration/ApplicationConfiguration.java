package ru.tsc.avramenko.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.tsc.avramenko.tm")
public class ApplicationConfiguration {
}