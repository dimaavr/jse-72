<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Task List</h1>

<table width="100%" cellpadding="12" border="1" style="margin-top: 25px">
    <tr>
        <th>ID</th>
        <th>User ID</th>
        <th>Project ID</th>
        <th>Name</th>
        <th>Desc</th>
        <th>Status</th>
        <th>Start Date</th>
        <th>Finish Date</th>
        <th>Create Date</th>
        <th align="center">Edit Project</th>
        <th align="center">Delete Project</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td align="center">
                <c:out value="${task.id}"/>
            </td>
            <td align="center">
                <c:out value="${task.userId}"/>
            </td>
            <td>
                <c:out value="${task.projectId}"/>
            </td>
            <td align="center">
                <c:out value="${task.name}"/>
            </td>
            <td align="center">
                <c:out value="${task.description}"/>
            </td>
            <td align="center">
                <c:out value="${task.status.getDisplayName()}"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${task.startDate}" pattern="dd.MM.yyyy - HH:mm"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${task.finishDate}" pattern="dd.MM.yyyy - HH:mm"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${task.created}" pattern="dd.MM.yyyy - HH:mm"/>
            </td>
            <td align="center">
                <a style="color: green" href="/task/edit/${task.id}">Edit</a>
            </td>
            <td align="center">
                <a style="color: red" href="/task/delete/${task.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" style="margin-top: 20px">
    <button>Create Task</button>
</form>

<jsp:include page="../include/_footer.jsp"/>